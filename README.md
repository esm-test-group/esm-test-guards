# Esm test guards

[![Pipeline status](https://gitlab.com/esm-test-group/esm-test-guards/badges/master/pipeline.svg)](https://gitlab.com/esm-test-group/esm-test-guards/-/pipelines)
[![The ISC license](https://img.shields.io/badge/license-ISC-orange.png?color=blue&style=flat-square)](http://opensource.org/licenses/ISC)
[![NPM version](https://img.shields.io/npm/v/@esm-test/guards.svg)](https://www.npmjs.org/package/@esm-test/guards)
[![NPM downloads](https://img.shields.io/npm/dm/@esm-test/guards.svg)](https://npmjs.org/package/@esm-test/guards "View this project on NPM")

[![BuyMeACoffee](https://www.buymeacoffee.com/assets/img/custom_images/purple_img.png)](https://www.buymeacoffee.com/peterf)

A js utility library for guarding objects, instances and types

```sh
npm install @esm-test/guards
```