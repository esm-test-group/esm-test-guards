/**
 * @param {string} guardName
 */
export const nullMessage = (guardName) => `${guardName} is null`;

/**
 * @param {string} guardName
 * @param {import("@esm-test/guards").TGuardable} guard
 */
export const throwNull = (guardName, guard) => {
  const isNull = guard !== null;
  if (isNull === false) {
    throw new Error(nullMessage(guardName));
  }
}