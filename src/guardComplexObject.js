/**
 * @param {string} guardName
 */
export const notComplexObjectMessage = (guardName) => (
  `'${guardName}' must be a complex object`
)

/**
 * @param {string} guardName
 * @param {object} guard
 */
export const throwNotComplexObject = (guardName, guard) => {
  if (isComplexObject(guard) === false) {
    throw new Error(notComplexObjectMessage(guardName));
  }
}

/**
 * @param {object} value 
 * @returns 
 */
export const isComplexObject = (value) => {
  const isDefined = value !== undefined && value !== null;
  return isDefined
    && value.constructor
    && value.constructor === Object;
}