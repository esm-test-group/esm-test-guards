/**
 * @param {string} guardName
 * @param {Object} guardType
 */
export const notInstanceMessage = (guardName, guardType) => (
  `'${guardName}' must be an instance of '${guardType.name}'`
)

/**
 * @param {string} guardName
 * @param {object} guard
 * @param {Object} guardType
 */
export const throwNotInstance = (guardName, guard, guardType) => {
  const isInstance = guard instanceof guardType;
  if (isInstance === false) {
    throw new Error(notInstanceMessage(guardName, guardType));
  }
}

/**
 * @param {object} guardInstance
 * @param {Object[]} guardTypes
 */
export const notAnyInstanceMessage = (guardInstance, ...guardTypes) => {
  const typeNames = guardTypes.map(t => t.name).join('|');
  return `'${guardInstance.constructor.name}' must be an instance of either '${typeNames}'`
}

/**
 * @param {object} guardInstance
 * @param {Object[]} guardTypes
 */
export const throwNotAnyInstance = (guardInstance, ...guardTypes) => {
  for (let i = 0; i < guardTypes.length; i++) {
    const isInstance = guardInstance instanceof guardTypes[i];
    if (isInstance) return;
  }
  throw new Error(
    notAnyInstanceMessage(guardInstance, ...guardTypes)
  );
}