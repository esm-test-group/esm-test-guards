/**
 * @param {string} guardName
 */
export const undefinedMessage = (guardName) => `${guardName} is not defined`;

/**
 * @param {string} guardName
 * @param {import("@esm-test/guards").TGuardable} guard
 */
export const throwUndefined = (guardName, guard) => {
  const isDefined = guard !== undefined;
  if (isDefined === false) {
    throw new Error(undefinedMessage(guardName));
  }
}