/**
 * @param {string} guardName
 * @param {string} guardType
 */
export const notTypeMessage = (guardName, guardType) => (
  `'${guardName}' must be a type of '${guardType}'`
)

/**
 * @param {string} guardName
 * @param {object} guard
 * @param {string} guardType
 */
export const throwNotType = (guardName, guard, guardType) => {
  const isType = typeof guard === guardType;
  if (isType === false) {
    throw new Error(notTypeMessage(guardName, guardType));
  }
}