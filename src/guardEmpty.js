/**
 * @param {string} guardName
 */
export const emptyMessage = (guardName) => `${guardName} is empty`;

/**
 * @param {string} guardName
 * @param {string} guard
 */
export const throwEmpty = (guardName, guard) => {
  const isEmpty = guard === '';
  if (isEmpty) {
    throw new Error(emptyMessage(guardName));
  }
}