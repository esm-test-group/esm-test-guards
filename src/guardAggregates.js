import { throwEmpty } from "./guardEmpty.js";
import { throwNull } from "./guardNull.js";
import { throwNotType } from "./guardType.js";
import { throwUndefined } from "./guardUndefined.js";

/**
 * @param {string} guardName
 * @param {import("@esm-test/guards").TGuardable} guard
 */
export const throwUndefinedOrNull = (guardName, guard) => {
  throwUndefined(guardName, guard);
  throwNull(guardName, guard);
}

/**
 * @param {string} guardName
 * @param {string} guard
 */
export const throwNotStringOrEmpty = (guardName, guard) => {
  throwNotType(guardName, guard, 'string')
  throwEmpty(guardName, guard);
}