/**
 * @param {string} guardName
 * @param {object} guard
 */
export const notObjectKeyMessage = (guardName, guard) => (
  `'${guardName}' must be one of '${Object.keys(guard).join(', ')}'`
)

/**
 * @param {string} guardName
 * @param {object} guard
 * @param {string} guardObjectKey
 */
export const throwNotObjectKey = (guardName, guard, guardObjectKey) => {
  const hasKey = Object.hasOwn(guard, guardObjectKey);
  if (hasKey === false) {
    throw new Error(notObjectKeyMessage(guardName, guard));
  }
}