# 1.0.0-beta.9

  - updated deps

# 1.0.0-beta.8

  - removed undefined and null checks from throwNotStringOrEmpty

# 1.0.0-beta.7

  - fixed index.d.ts fn name for throwNotStringOrEmpty

# 1.0.0-beta.6

- added throwEmpty (for strings)

- added aggregate guards
  - throwUndefinedOrNull
  - throwNotSetOrEmpty

# 1.0.0-beta.5

- added cjs dist output

# 1.0.0-beta.3

- moved package to @esm-test npm org

# 1.0.0-beta.2

- Fixed package repo link

# 1.0.0-beta.1

- init commit extracted from [esm-test-parser](https://gitlab.com/esm-test-group/esm-test-parser)