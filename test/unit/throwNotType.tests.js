import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { throwNotType, notTypeMessage } from '../../src/index.js';

export const throwNotTypeTests = {

  [test.title]: throwNotType.name,

  'throws an error when instanceof is false': [
    [123, "object"],
    [Error, "string"],
    function (testInstance, testGuardType) {
      try {
        throwNotType("testName", testInstance, testGuardType)
        assert.ok(false)
      } catch (error) {
        assert.equal(
          error.message,
          notTypeMessage("testName", testGuardType)
        )
      }
    }
  ],

  'does not throw an error when instanceof is true': [
    [new Object(), "object"],
    ["", "string"],
    [1, "number"],
    function (testInstance, testGuardType) {
      try {
        throwNotType("testName", testInstance, testGuardType)
        assert.ok(true)
      } catch (error) {
        assert.ok(false, error.message)
      }
    }
  ],

}