import * as assert from 'assert';
import { test } from 'esm-test-parser';
import {
  notInstanceMessage,
  throwNotInstance
} from '../../src/index.js';

export const throwNotInstanceTests = {

  [test.title]: throwNotInstance.name,

  'throws an error when instanceof does not match': [
    [new Object(), Boolean],
    [new Boolean, Error],
    function (testInstance, testGuardType) {
      try {
        throwNotInstance("test", testInstance, testGuardType)
        assert.ok(false)
      } catch (error) {
        assert.equal(
          error.message,
          notInstanceMessage("test", testGuardType)
        )
      }
    }
  ],

  'does not throw an error when instanceof matches': [
    [new Boolean, Boolean],
    [new Object(), Object],
    function (testInstance, testGuardType) {
      try {
        throwNotInstance("test", testInstance, testGuardType)
        assert.ok(true)
      } catch (error) {
        assert.ok(false, error.message)
      }
    }
  ],

}