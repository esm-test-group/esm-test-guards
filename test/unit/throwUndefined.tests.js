import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { throwUndefined, undefinedMessage } from '../../src/index.js';

export const throwUndefinedTests = {

  [test.title]: throwUndefined.name,

  'throws an error when the value is undefined': () => {
    try {
      throwUndefined("testArgName", undefined)
      assert.ok(false)
    } catch (error) {
      assert.equal(
        error.message,
        undefinedMessage("testArgName")
      )
    }
  },

  'does not throw an error when the value is defined': () => {
    try {
      throwUndefined("testArgName", 1)
      assert.ok(true)
    } catch (error) {
      assert.ok(false, error.message)
    }
  },

}