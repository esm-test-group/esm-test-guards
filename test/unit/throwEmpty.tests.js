import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { emptyMessage, throwEmpty } from '../../src/index.js';

export const throwEmptyTests = {

  [test.title]: throwEmpty.name,

  'throws an error when the string value is empty': () => {
    try {
      throwEmpty("testArgName", '')
      assert.ok(false)
    } catch (error) {
      assert.equal(
        error.message,
        emptyMessage("testArgName")
      )
    }
  },

  'does not throw an error when the string value is not empty': () => {
    try {
      throwEmpty("testArgName", "not empty string")
      assert.ok(true)
    } catch (error) {
      assert.ok(false, error.message)
    }
  },

}