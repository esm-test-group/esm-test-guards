import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { throwNull, nullMessage } from '../../src/index.js';

export const throwNullTests = {

  [test.title]: throwNull.name,

  'throws an error when the value is null': () => {
    try {
      throwNull("testArgName", null)
      assert.ok(false)
    } catch (error) {
      assert.equal(
        error.message,
        nullMessage("testArgName")
      )
    }
  },

  'does not throw an error when the value is not null': () => {
    try {
      throwNull("testArgName", 1)
      assert.ok(true)
    } catch (error) {
      assert.ok(false, error.message)
    }
  },

}