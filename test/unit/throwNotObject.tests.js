import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { throwNotObjectKey, notObjectKeyMessage } from '../../src/index.js';

export const throwNotObjectTests = {

  [test.title]: throwNotObjectKey.name,

  beforeEach: function () {
    this.testObject = {
      "item1": 1,
      "item2": 2
    };
  },

  'throws an error when key does not exist': [
    "object",
    "string",
    "item3",
    function (testKey) {
      try {
        throwNotObjectKey("testKey", this.testObject, testKey)
        assert.ok(false)
      } catch (error) {
        assert.equal(
          error.message,
          notObjectKeyMessage("testKey", this.testObject)
        )
      }
    }
  ],

  'does not throw an error for valid keys': [
    "item1",
    "item2",
    function (testKey) {
      try {
        throwNotObjectKey("testKey", this.testObject, testKey)
        assert.ok(true)
      } catch (error) {
        assert.ok(false, error.message)
      }
    }
  ],

}