import * as assert from 'assert';
import { test } from 'esm-test-parser';
import {
  notAnyInstanceMessage,
  throwNotAnyInstance
} from '../../src/index.js';

export const throwNotAnyInstanceTests = {

  [test.title]: throwNotAnyInstance.name,

  'throws an error when all instanceof checks are false': [
    [new Object(), [Boolean, Error]],
    [new Boolean, [Error, Float32Array]],
    function (testInstance, testGuardTypes) {
      try {
        throwNotAnyInstance(testInstance, ...testGuardTypes)
        assert.ok(false)
      } catch (error) {
        assert.equal(
          error.message,
          notAnyInstanceMessage(testInstance, ...testGuardTypes)
        )
      }
    }
  ],

  'does not throw an error when any instanceof check is true': [
    [new Object(), [Object, Error]],
    [new Boolean, [Object, Boolean]],
    function (testInstance, testGuardTypes) {
      try {
        throwNotAnyInstance(testInstance, ...testGuardTypes)
        assert.ok(true)
      } catch (error) {
        assert.ok(false, error.message)
      }
    }
  ],

}