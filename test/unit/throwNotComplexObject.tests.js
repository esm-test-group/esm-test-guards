import * as assert from 'assert';
import { test } from 'esm-test-parser';
import {
  notComplexObjectMessage,
  throwNotComplexObject
} from '../../src/guardComplexObject.js';

export const throwNotComplexObjectTests = {

  [test.title]: throwNotComplexObject.name,

  'throws an error when the value is "$2"': [
    [undefined, "undefined"],
    [null, "null"],
    [[], "empty array"],
    [["item1", "item2"], "array of items"],
    [0, "number"],
    ["", "string"],
    [new Error(), "new Error()"],
    [Object, "Object"],
    [() => true, "Function"],
    function (testValue) {
      try {
        throwNotComplexObject("testArgName", testValue)
        assert.ok(false)
      } catch (error) {
        assert.equal(
          error.message,
          notComplexObjectMessage("testArgName")
        )
      }
    }
  ],

  'does not throw an error when the value is "$2"': [
    [{}, "{}"],
    [new Object(), "new Object()"],
    function (testValue) {
      try {
        throwNotComplexObject("testArgName", testValue)
        assert.ok(true)
      } catch (error) {
        assert.ok(false, error.message)
      }
    }
  ]

}