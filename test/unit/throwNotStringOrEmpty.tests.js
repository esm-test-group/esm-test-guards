import { emptyMessage, notTypeMessage, nullMessage, undefinedMessage } from '@esm-test/guards';
import * as assert from 'assert';
import { test } from 'esm-test-parser';
import { throwNotStringOrEmpty } from '../../src/index.js';

export const throwNotStringOrEmptyTests = {

  [test.title]: throwNotStringOrEmpty.name,

  'throws an error when the string is $1': [
    [undefined, notTypeMessage('testArgName', 'string')],
    [null, notTypeMessage('testArgName', 'string')],
    [{}, notTypeMessage('testArgName', 'string')],
    [true, notTypeMessage('testArgName', 'string')],
    [1, notTypeMessage('testArgName', 'string')],
    ['', emptyMessage('testArgName')],
    (testString, expectedMessage) => {
      try {
        throwNotStringOrEmpty("testArgName", testString)
        assert.ok(false)
      } catch (error) {
        assert.equal(error.message, expectedMessage)
      }
    }
  ],

  'does not throw an error when the value is a set string': () => {
    try {
      throwNotStringOrEmpty("testArgName", "not empty string")
      assert.ok(true)
    } catch (error) {
      assert.ok(false, error.message)
    }
  }

}