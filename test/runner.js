import Mocha from 'mocha';
import registerMochaUiEsm from 'mocha-ui-esm';
import SourceMaps from 'source-map-support';
import * as UnitTestModules from './unit/all.tests.js';

// register esm test intergration
registerMochaUiEsm();

// create the test runner
const runner = new Mocha({
  ui: "esm",
  reporter: 'spec',
  color: true,
  timeout: 60000,
})

// register tests
runner.suite.emit("modules", UnitTestModules)

// register soucemap support
SourceMaps.install();

// execute the tests
runner.run(
  failures => {
    if (process) process.exit(failures)
  }
)